use error::Result;
use regex::Regex;
use std::collections::HashMap;

fn calc(input: &Vec<&str>) -> Result<HashMap<usize, [usize; 60]>> {
    let re_guard = Regex::new(r"\[1518-(?P<month>.*)-(?P<day>.*) (?P<hour>.*):(?P<minute>.*)\] Guard #(?P<guard>.*) begins shift")?;
    let re_asleep =
        Regex::new(r"\[1518-(?P<month>.*)-(?P<day>.*) (?P<hour>.*):(?P<minute>.*)\] falls asleep")?;
    let re_wakeup =
        Regex::new(r"\[1518-(?P<month>.*)-(?P<day>.*) (?P<hour>.*):(?P<minute>.*)\] wakes up")?;

    let mut timings = HashMap::new();

    let mut guard = 0;
    let mut start = 0;
    let mut end = 0;

    for line in input {
        if let Some(caps) = re_guard.captures(line) {
            guard = caps.name("guard").expect("no match").parse::<usize>()?;
            start = 0;
            end = 0;
        }
        if let Some(caps) = re_asleep.captures(line) {
            start = caps.name("minute").expect("no match").parse::<usize>()?;
        }
        if let Some(caps) = re_wakeup.captures(line) {
            end = caps.name("minute").expect("no match").parse::<usize>()?;
        }

        if start > 0 && end > 0 && end > start {
            let entry = timings.entry(guard).or_insert([0usize; 60]);
            for min in start..end {
                entry[min] += 1;
            }
        }
    }

    Ok(timings)
}

fn p1(input: &Vec<&str>) -> usize {
    let timings = calc(input).expect("calc gone wrong");
    let mut guard = 0;
    let mut entry = [0; 60];
    let mut max = 0;

    for (k, v) in timings {
        let sum: usize = v.iter().sum();
        if sum > max {
            max = sum;
            guard = k;
            entry = v;
        }
    }

    let mut max_min = 0;
    max = 0;
    for min in 0..60 {
        if entry[min] > max {
            max = entry[min];
            max_min = min;
        }
    }

    guard * max_min
}

fn p2(input: &Vec<&str>) -> usize {
    let timings = calc(input).expect("calc gone wrong");
    let mut guard = 0;
    let mut max = 0;
    let mut max_min = 0;

    for (g, ent) in timings {
        for min in 0..60 {
            if ent[min] > max {
                max = ent[min];
                max_min = min;
                guard = g;
            }
        }
    }

    guard * max_min
}

pub fn run() {
    let t = "[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up"
        .lines()
        .map(|l| l.into())
        .collect();

    assert_eq!(240, p1(&t));
    assert_eq!(4455, p2(&t));

    let input = super::input(4);
    let mut input: Vec<&str> = input.iter().map(|s| s.as_ref()).collect();
    input.sort_unstable();

    println!("I: {:?}", p1(&input));
    println!("II: {:?}", p2(&input));
}
