use std::collections::HashSet;

fn numbers(input: Vec<String>) -> Vec<i32> {
    input
        .iter()
        .map(|line| line.parse::<i32>().expect("not a number"))
        .collect()
}

fn p1(input: &Vec<i32>) -> i32 {
    input.iter().sum()
}

fn p2(input: &Vec<i32>) -> i32 {
    let mut soln = 0;
    let mut freqs = HashSet::new();
    freqs.insert(soln);

    for num in input.iter().cycle() {
        soln += num;

        if freqs.contains(&soln) {
            break;
        }
        freqs.insert(soln);
    }

    soln
}

pub fn run() {
    let input = super::input(1);
    let input = numbers(input);

    println!("I: {:?}", p1(&input));

    assert_eq!(2, p2(&vec![1, -2, 3, 1]));
    assert_eq!(0, p2(&vec![1, -1]));
    assert_eq!(10, p2(&vec![3, 3, 4, -2, -4]));
    assert_eq!(5, p2(&vec![-6, 3, 8, 5, -6]));
    assert_eq!(14, p2(&vec![7, 7, -2, -7, -4]));
    println!("II: {:?}", p2(&input));
}
