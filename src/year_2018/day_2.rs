use std::collections::HashMap;

fn p1(input: &Vec<&str>) -> i32 {
    let mut threes = 0;
    let mut twos = 0;

    for line in input {
        let mut coll = HashMap::new();
        for ch in line.chars() {
            let ent = coll.entry(ch).or_insert(0);
            *ent += 1;
        }

        if coll.iter().any(|(_k, v)| *v == 3) {
            threes += 1;
        }
        if coll.iter().any(|(_k, v)| *v == 2) {
            twos += 1;
        }
    }

    threes * twos
}

fn p2(input: &Vec<&str>) -> String {
    let mut input = input.clone();
    input.sort_unstable();

    for el in input.as_slice().windows(2) {
        let vch1 = el[0].chars();
        let vch2 = el[1].chars();

        let common = vch1
            .zip(vch2)
            .filter_map(|(ch1, ch2)| if ch1 == ch2 { Some(ch1) } else { None })
            .collect::<String>();

        if el[0].len() - 1 == common.len() {
            return common;
        }
    }

    unreachable!()
}

pub fn run() {
    let input = super::input(2);
    let input = input.iter().map(|s| s.as_ref()).collect();

    assert_eq!(
        12,
        p1(&vec![
            "abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab"
        ])
    );
    println!("I: {:?}", p1(&input));

    assert_eq!(
        "fgij",
        p2(&vec![
            "abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz"
        ])
    );
    println!("II: {:?}", p2(&input));
}
