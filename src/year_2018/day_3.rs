use regex::Regex;

fn p1(input: &Vec<&str>) -> usize {
    let re_inst =
        Regex::new(r"#(?P<num>.*) @ (?P<left>.*),(?P<top>.*): (?P<width>.*)x(?P<height>.*)")
            .unwrap();
    let mut area = [[0; 1024]; 1024];
    let mut overlaps = 0;

    for line in input {
        let caps = re_inst.captures(line).unwrap();
        let _claim_no = caps.name("num").unwrap();
        let left = caps
            .name("left")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");
        let top = caps
            .name("top")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");
        let width = caps
            .name("width")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");
        let height = caps
            .name("height")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");

        for x in left..left + width {
            for y in top..top + height {
                area[x][y] += 1;
                if area[x][y] == 2 {
                    overlaps += 1;
                }
            }
        }
    }

    overlaps
}

fn p2(input: &Vec<&str>) -> usize {
    let re_inst =
        Regex::new(r"#(?P<num>.*) @ (?P<left>.*),(?P<top>.*): (?P<width>.*)x(?P<height>.*)")
            .unwrap();
    let mut area = [[0; 1024]; 1024];

    for line in input {
        let caps = re_inst.captures(line).unwrap();
        let _claim_no = caps
            .name("num")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");
        let left = caps
            .name("left")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");
        let top = caps
            .name("top")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");
        let width = caps
            .name("width")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");
        let height = caps
            .name("height")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");

        for x in left..left + width {
            for y in top..top + height {
                area[x][y] += 1;
            }
        }
    }

    'next_claim: for line in input {
        let caps = re_inst.captures(line).unwrap();
        let claim_no = caps
            .name("num")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");
        let left = caps
            .name("left")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");
        let top = caps
            .name("top")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");
        let width = caps
            .name("width")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");
        let height = caps
            .name("height")
            .unwrap()
            .parse::<usize>()
            .expect("not a number");

        for x in left..left + width {
            for y in top..top + height {
                if area[x][y] > 1 {
                    continue 'next_claim;
                }
            }
        }

        return claim_no;
    }

    unreachable!()
}

pub fn run() {
    let input = super::input(3);
    let input = input.iter().map(|s| s.as_ref()).collect();

    assert_eq!(
        4,
        p1(&vec!["#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"])
    );
    println!("I: {:?}", p1(&input));

    assert_eq!(
        3,
        p2(&vec!["#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"])
    );
    println!("II: {:?}", p2(&input));
}
