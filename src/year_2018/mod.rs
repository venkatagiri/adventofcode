pub fn input(day: u32) -> Vec<String> {
    super::input(2018, day)
}

pub mod day_1;
pub mod day_2;
pub mod day_3;
pub mod day_4;
