use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashMap, HashSet};
use std::vec::IntoIter;

#[derive(PartialEq, Eq, Clone, Debug, Hash)]
struct GridState {
    elevator: i32,
    floors: Vec<u16>,
}

#[derive(PartialEq, Eq, Debug)]
struct Node {
    state: GridState,
    cost: i32,
}

impl Ord for Node {
    fn cmp(&self, other: &Node) -> Ordering {
        other.cost.cmp(&self.cost) // comparison is reversed
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Node) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn ones(n: &u16) -> i32 {
    format!("{:b}", n)
        .chars()
        .fold(0i32, |sum, val| sum + val.to_digit(10).unwrap() as i32)
}

fn cartesian_product(v1: &u16, v2: &u16) -> Vec<u16> {
    let mut result = HashSet::new();
    for s1 in 0..16 {
        let el1 = v1 & (1 << s1);
        if el1 == 0 {
            continue;
        }
        for s2 in 0..16 {
            let el2 = v2 & (1 << s2);
            if el2 == 0 {
                continue;
            }
            result.insert(el1 | el2);
        }
    }
    result.into_iter().collect()
}

fn heuristic(state: &GridState) -> i32 {
    let mut h = 0;
    for (i, f) in state.floors.iter().rev().enumerate() {
        h += ones(f) * i as i32
    }
    (h as f32 / 2.0).ceil() as i32
}

fn combos(floor: &u16) -> Vec<u16> {
    let mut cs = HashSet::new();
    for fs in cartesian_product(&floor, &floor) {
        cs.insert(fs);
    }
    for shift in 0..16 {
        let el = floor & 1 << shift;
        if el > 0 {
            cs.insert(el);
        }
    }
    cs.into_iter().collect()
}

fn is_valid_floor(floor: &u16) -> bool {
    let mut genc = 0;
    for shift in 8..16 {
        if floor & 1 << shift > 1 {
            genc += 1;
        }
    }
    if genc == 0 {
        return true;
    }
    for shift in 0..8 {
        let chip = floor & 1 << shift;
        if chip == 0 {
            continue;
        }
        if floor & chip << 8 == 0 {
            return false;
        }
    }
    true
}

fn moves(state: &GridState) -> IntoIter<GridState> {
    let mut vec = vec![];
    for &nlevel in [state.elevator - 1, state.elevator + 1].iter() {
        // check for valid move to a floor
        if nlevel < 0 || state.floors.len() as i32 == nlevel {
            continue;
        }
        for stuff in combos(&state.floors[state.elevator as usize]) {
            let mut nfloors = state.floors.clone();
            for shift in 0..16 {
                let el = stuff & (1 << shift);
                if el == 0 {
                    continue;
                }
                nfloors[state.elevator as usize] &= !(1 << shift); // remove el from here
                nfloors[nlevel as usize] |= 1 << shift; // add el from here
            }
            if is_valid_floor(&nfloors[state.elevator as usize])
                && is_valid_floor(&nfloors[nlevel as usize])
            {
                vec.push(GridState {
                    elevator: nlevel,
                    floors: nfloors,
                });
            }
        }
    }
    vec.into_iter()
}

fn astar_search(start: &GridState) -> Option<Vec<GridState>> {
    let mut heap = BinaryHeap::new();
    heap.push(Node {
        cost: heuristic(start),
        state: start.clone(),
    });

    let mut previous: HashMap<GridState, GridState> = HashMap::new();
    let mut path_cost = HashMap::new();
    path_cost.insert(start.clone(), 0);

    while let Some(node) = heap.pop() {
        if heuristic(&node.state) == 0 {
            let mut path = vec![node.state.clone()];
            let mut ends = node.state.clone();
            while let Some(ref end) = previous.get(&ends) {
                path.push((*end).clone());
                ends = (*end).clone();
            }
            return Some(path);
        }
        for state in moves(&node.state) {
            let ncost = path_cost.get(&node.state).unwrap() + 1;
            if !path_cost.contains_key(&state) || ncost < path_cost.get(&state).unwrap().clone() {
                heap.push(Node {
                    cost: ncost + heuristic(&state),
                    state: state.clone(),
                });
                {
                    let val = path_cost.entry(state.clone()).or_insert(0);
                    *val = ncost;
                }
                previous.insert(state.clone(), node.state.clone());
            }
        }
    }
    None
}

fn find(floors: &Vec<u16>) -> usize {
    let gs = GridState {
        elevator: 0,
        floors: floors.clone(),
    };
    let path = astar_search(&gs).expect("path not found");
    path.len() - 1
}

pub fn run() {
    let mut floors = vec![
        1 | 1 << 8 | 2 << 8 | 4 << 8,
        2 | 4,
        8 | 8 << 8 | 16 | 16 << 8,
        0,
    ];

    println!("p1 is {}", find(&floors));
    floors[0] |= 32 | 32 << 8 | 64 | 64 << 8;
    println!("p2 is {}", find(&floors));
}
