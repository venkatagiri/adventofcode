fn find(no_of_elves: u32, across: bool) -> u32 {
    let mut elves = vec![];
    for n in 1..no_of_elves + 1 {
        elves.push(n);
    }
    let mut to = 0usize;
    while elves.len() > 1 {
        // println!("elves are {:?}", elves);
        let from = if !across {
            (to + 1) % elves.len()
        } else {
            let len = elves.len() - 1;
            let offset = if len % 2 == 0 { len / 2 } else { len / 2 + 1 };
            (to + offset) % elves.len()
        };
        // println!("{} takes {} presents", elves[to], elves[from]);

        elves.remove(from);

        if to == elves.len() {
            to = 0;
        } else {
            to = (to + 1) % elves.len();
        }
    }
    elves[0]
}

pub fn run() {
    assert_eq!(find(5, false), 3);
    println!("p1 is {}", find(3012210, false));
    assert_eq!(find(5, true), 2);
    println!("p2 is {}", find(3012210, true));
}
