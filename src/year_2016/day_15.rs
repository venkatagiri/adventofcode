use regex::Regex;

fn part1(input: &Vec<String>) -> u32 {
    let re = Regex::new(r"Disc #(?P<disk>\d+) has (?P<total_positions>\d+) positions; at time=(?P<time>\d+), it is at position (?P<current_position>\d+).").unwrap();
    let mut disks = vec![];
    for line in input {
        let caps = re.captures(line).unwrap();
        // let disk: u32 = caps.name("disk").unwrap().parse().unwrap();
        let total_positions: u32 = caps.name("total_positions").unwrap().parse().unwrap();
        let time: u32 = caps.name("time").unwrap().parse().unwrap();
        let current_position: u32 = caps.name("current_position").unwrap().parse().unwrap();
        disks.push((total_positions, time, current_position));
    }
    let no_of_disks = disks.len();
    for t in 0.. {
        for dt in 1..no_of_disks + 1 {
            if (disks[dt - 1].2 + t + dt as u32) % disks[dt - 1].0 == 0 {
                if dt == no_of_disks {
                    return t;
                }
            } else {
                break;
            }
        }
    }
    999
}

pub fn run() {
    let mut input: Vec<String> = {
        use std::fs::File;
        use std::io::prelude::*;
        use std::io::BufReader;

        let file = BufReader::new(
            File::open("input/year_2016/day_15.txt").expect("file (input.txt) not found"),
        );
        file.lines().filter_map(|l| l.ok()).collect()
    };
    println!("p1 is {}", part1(&input));
    input.push("Disc #7 has 11 positions; at time=0, it is at position 0.".to_string());
    println!("p2 is {}", part1(&input));
}
