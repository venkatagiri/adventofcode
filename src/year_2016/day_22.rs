use astar::{astar, SearchProblem};
use regex::Regex;
use std::vec::IntoIter;

#[derive(Clone)]
struct Disk {
    pos: (i32, i32),
    size: i32,
    used: i32,
    avail: i32,
}

fn cap_to_i32(cap: Option<&str>) -> i32 {
    cap.unwrap().parse().unwrap()
}

struct GridState {
    grid: Vec<Disk>,
    data: (i32, i32),
    empty: (i32, i32),
}

impl SearchProblem for GridState {
    type Node = (i32, i32);
    type Cost = i32;
    type Iter = IntoIter<((i32, i32), i32)>;

    fn start(&self) -> (i32, i32) {
        self.empty
    }
    fn is_end(&self, other: &(i32, i32)) -> bool {
        other == &self.data
    }
    fn heuristic(&self, &(p_x, p_y): &(i32, i32)) -> i32 {
        let (s_x, s_y) = self.data;
        (s_x - p_x).abs() + (s_y - p_y).abs()
    }
    fn neighbors(&mut self, &(x, y): &(i32, i32)) -> IntoIter<((i32, i32), i32)> {
        let mut vec = vec![];
        for &(dx, dy) in [(1, 0), (-1, 0), (0, 1), (0, -1)].iter() {
            let nx = x + dx;
            let ny = y + dy;

            if self
                .grid
                .iter()
                .position(|&ref g| g.pos == (nx, ny))
                .is_none()
            {
                continue;
            }

            let ed = self
                .grid
                .iter()
                .filter(|d| d.pos == self.empty)
                .nth(0)
                .unwrap();
            let pd = self
                .grid
                .iter()
                .filter(|d| d.pos == (nx, ny))
                .nth(0)
                .unwrap();

            if pd.used < ed.size {
                vec.push((pd.pos, 1));
            }
        }
        vec.into_iter()
    }
}

fn part2(disks: &Vec<Disk>) {
    let grid = disks.to_vec();
    let empty = grid.iter().filter(|d| d.used == 0).nth(0).unwrap();
    let max_x = {
        let mut xs = grid.iter().map(|d| (d.pos).0).collect::<Vec<_>>();
        xs.sort();
        xs.last().unwrap().clone()
    };
    let mut gs = GridState {
        data: (max_x, 0),
        empty: empty.pos,
        grid: grid.to_vec(),
    };
    let path = astar(&mut gs).unwrap();
    println!("p2 is {}", (path.len() as i32 - 1) + (max_x - 1) * 5); // needs 5 moves to move D towards G
}

fn part1(disks: &Vec<Disk>) {
    let mut viable = 0;
    for d1 in disks {
        for d2 in disks {
            if d1.used != 0 && d1.pos != d2.pos && d1.used <= d2.avail {
                viable += 1;
            }
        }
    }
    println!("p1 is {}", viable);
}

pub fn run() {
    let input = super::input(22);
    let re = Regex::new(r"/dev/grid/node-x(?P<x>\d+)-y(?P<y>\d+)\s+(?P<size>\d+)T\s+(?P<used>\d+)T\s+(?P<avail>\d+)T").unwrap();
    let disks: Vec<Disk> = input
        .iter()
        .filter_map(|line| {
            if let Some(caps) = re.captures(line) {
                let x = cap_to_i32(caps.name("x"));
                let y = cap_to_i32(caps.name("y"));
                let size = cap_to_i32(caps.name("size"));
                let used = cap_to_i32(caps.name("used"));
                let avail = cap_to_i32(caps.name("avail"));
                Some(Disk {
                    pos: (x, y),
                    size: size,
                    used: used,
                    avail: avail,
                })
            } else {
                None
            }
        })
        .collect();
    part1(&disks);
    part2(&disks);
}
