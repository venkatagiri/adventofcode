use astar::{astar, SearchProblem};
use std::collections::{HashMap, VecDeque};
use std::vec::IntoIter;

fn is_open(&(x, y): &(i32, i32)) -> bool {
    let val = x * x + 3 * x + 2 * x * y + y + y * y + 1350;
    let bits = format!("{:b}", val);
    let ones = bits
        .chars()
        .fold(0i32, |sum, val| sum + val.to_digit(10).unwrap() as i32);
    ones % 2 == 0
}

fn neighbours(&(x, y): &(i32, i32)) -> IntoIter<((i32, i32), i32)> {
    let mut vec = vec![];
    for &(dx, dy) in [(1, 0), (-1, 0), (0, 1), (0, -1)].iter() {
        if (x + dx) >= 0 && (y + dy) >= 0 && is_open(&(x + dx, y + dy)) {
            vec.push(((x + dx, y + dy), 1));
        }
    }
    vec.into_iter()
}

struct GridState {
    start: (i32, i32),
    end: (i32, i32),
}

impl SearchProblem for GridState {
    type Node = (i32, i32);
    type Cost = i32;
    type Iter = IntoIter<((i32, i32), i32)>;

    fn start(&self) -> (i32, i32) {
        self.start
    }
    fn is_end(&self, other: &(i32, i32)) -> bool {
        other == &self.end
    }
    fn heuristic(&self, &(p_x, p_y): &(i32, i32)) -> i32 {
        let (s_x, s_y) = self.end;
        (s_x - p_x).abs() + (s_y - p_y).abs()
    }
    fn neighbors(&mut self, loc: &(i32, i32)) -> IntoIter<((i32, i32), i32)> {
        neighbours(loc)
    }
}

fn part1() {
    let mut gs = GridState {
        start: (1, 1),
        end: (31, 39),
    };
    let path = astar(&mut gs).unwrap();
    println!("p1 is {}", path.len() - 1);
}

fn part2() {
    let mut q = VecDeque::new();
    let mut distance: HashMap<(i32, i32), i32> = HashMap::new();
    q.push_back((1, 1));
    while let Some(s) = q.pop_front() {
        let d = distance.entry(s).or_insert(0).clone();
        if d < 50 {
            for (s2, _) in neighbours(&s) {
                if !distance.contains_key(&s2) {
                    q.push_back(s2);
                    distance.insert(s2, d + 1);
                }
            }
        }
    }
    println!("p2 is {:?}", distance.len());
}

pub fn run() {
    part1();
    part2();
}
