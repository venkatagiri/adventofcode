use crypto::digest::Digest;
use crypto::md5::Md5;

pub fn run() {
    let mut hasher = Md5::new();

    let key = "cxdnnyjw".as_bytes();

    let mut hash = [0; 16];
    let mut result = vec![];

    for x in 0..::std::u64::MAX {
        hasher.input(key);
        hasher.input(x.to_string().as_bytes());
        hasher.result(&mut hash);

        if hash[0] == 0 && hash[1] == 0 && hash[2] < 16 {
            result.push(hash[2]);
            if result.len() == 8 {
                break;
            }
        }

        hasher.reset();
    }

    print!("answer 1 is ");
    for x in result {
        print!("{:x}", x);
    }
    println!("");

    let mut result2 = vec![-1; 8];
    let mut found = 0;
    hasher.reset();

    for x in 0..::std::u64::MAX {
        hasher.input(key);
        hasher.input(x.to_string().as_bytes());
        hasher.result(&mut hash);

        if hash[0] == 0 && hash[1] == 0 && hash[2] < 16 {
            let pos = hash[2] as usize;
            if pos < 8 && result2[pos] == -1 {
                result2[pos] = hash[3] as i32 >> 4;
                found += 1;
                if found == 8 {
                    break;
                }
            }
        }

        hasher.reset();
    }

    print!("answer 2 is ");
    for x in result2 {
        print!("{:x}", x);
    }
    println!("");
}
