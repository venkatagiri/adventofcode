pub fn run() {
    let mut list = super::input(20)
        .iter()
        .map(|s| {
            let parts: Vec<&str> = s.split('-').collect();
            (parts[0].parse().unwrap(), parts[1].parse().unwrap())
        })
        .collect::<Vec<(u64, u64)>>();
    list.sort_by(|&(a, _), &(b, _)| a.cmp(&b));

    let mut soln = 0;
    let mut i: u64 = 0;
    'outer: for &(min, max) in &list {
        for x in i..min {
            soln = x;
            break 'outer;
        }
        if max > i {
            i = max + 1;
        }
    }
    println!("p1 is {}", soln);

    soln = 0;
    i = 0;
    for &(min, max) in &list {
        for _ in i..min {
            soln += 1;
        }
        if max + 1 > i {
            i = max + 1;
        }
    }
    println!("p2 is {}", soln);
}
