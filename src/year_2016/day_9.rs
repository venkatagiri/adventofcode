fn explode(input: &str) -> String {
    let mut output = String::new();
    let input: Vec<char> = input.chars().collect();
    let mut x = 0;
    while x < input.len() {
        if input[x] != '(' {
            output.push(input[x]);
            x += 1;
        } else {
            x += 1;
            let mut no_of_chars = vec![];
            while input[x] != 'x' {
                no_of_chars.push(input[x]);
                x += 1;
            }
            let mut repeat = vec![];
            x += 1;
            while input[x] != ')' {
                repeat.push(input[x]);
                x += 1;
            }
            x += 1;
            let no_of_chars = no_of_chars
                .into_iter()
                .collect::<String>()
                .parse::<usize>()
                .unwrap();
            let repeat = repeat
                .into_iter()
                .collect::<String>()
                .parse::<usize>()
                .unwrap();
            let chars = {
                let mut coll = vec![];
                for y in 0..no_of_chars {
                    coll.push(input[x + y]);
                }
                coll.into_iter().collect::<String>()
            };
            for _ in 0..repeat {
                output.push_str(&chars);
            }
            x += no_of_chars;
        }
    }
    output
}

fn explode_length(input: &str) -> usize {
    let mut length = 0;
    let input: Vec<char> = input.chars().collect();
    let mut x = 0;
    while x < input.len() {
        if input[x] != '(' {
            length += 1;
            x += 1;
        } else {
            x += 1;
            let mut no_of_chars = vec![];
            while input[x] != 'x' {
                no_of_chars.push(input[x]);
                x += 1;
            }
            let mut repeat = vec![];
            x += 1;
            while input[x] != ')' {
                repeat.push(input[x]);
                x += 1;
            }
            x += 1;
            let no_of_chars = no_of_chars
                .into_iter()
                .collect::<String>()
                .parse::<usize>()
                .unwrap();
            let repeat = repeat
                .into_iter()
                .collect::<String>()
                .parse::<usize>()
                .unwrap();
            let chars = {
                let mut coll = vec![];
                for y in 0..no_of_chars {
                    coll.push(input[x + y]);
                }
                coll.into_iter().collect::<String>()
            };
            length += repeat * explode_length(&chars);
            x += no_of_chars;
        }
    }
    length
}

pub fn run() {
    let input: String = {
        use std::fs::File;
        use std::io::prelude::*;
        use std::io::BufReader;

        let mut file = BufReader::new(
            File::open("input/year_2016/day_9.txt").expect("file (input.txt) not found"),
        );
        let mut line = String::new();
        file.read_line(&mut line).unwrap();
        line
    };

    assert_eq!(explode("ADVENT"), "ADVENT");
    assert_eq!(explode("A(1x5)BC"), "ABBBBBC");
    assert_eq!(explode("(3x3)XYZ"), "XYZXYZXYZ");
    assert_eq!(explode("A(2x2)BCD(2x2)EFG"), "ABCBCDEFEFG");
    assert_eq!(explode("(6x1)(1x3)A"), "(1x3)A");
    assert_eq!(explode("X(8x2)(3x3)ABCY"), "X(3x3)ABC(3x3)ABCY");
    println!("p1 is {}", explode(&input).len());
    assert_eq!(
        explode_length("(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN"),
        445
    );
    println!("p2 is {}", explode_length(&input));
}
