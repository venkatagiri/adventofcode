use std::cmp::PartialEq;

#[derive(Debug, Clone)]
struct Position {
    x: i32,
    y: i32,
}

impl Position {
    fn new() -> Position {
        Position { x: 0, y: 0 }
    }
    fn go_north(&mut self) {
        self.y += 1;
    }
    fn go_south(&mut self) {
        self.y -= 1;
    }
    fn go_east(&mut self) {
        self.x += 1;
    }
    fn go_west(&mut self) {
        self.x -= 1;
    }
}

impl PartialEq for Position {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

#[derive(Debug)]
enum Bearing {
    NORTH,
    EAST,
    SOUTH,
    WEST,
}

fn calc_new_bearing(bearing: Bearing, dir: &str) -> Bearing {
    if dir == "L" {
        match bearing {
            Bearing::NORTH => Bearing::WEST,
            Bearing::EAST => Bearing::NORTH,
            Bearing::SOUTH => Bearing::EAST,
            Bearing::WEST => Bearing::SOUTH,
        }
    } else {
        match bearing {
            Bearing::NORTH => Bearing::EAST,
            Bearing::EAST => Bearing::SOUTH,
            Bearing::SOUTH => Bearing::WEST,
            Bearing::WEST => Bearing::NORTH,
        }
    }
}

pub fn run() {
    let input = {
        use std::fs::File;
        use std::io::prelude::*;
        use std::io::BufReader;

        let mut file = BufReader::new(File::open("input/year_2016/day_1.txt").unwrap());
        let mut buffer = String::new();
        file.read_line(&mut buffer).unwrap();
        buffer
    };

    let directions: Vec<&str> = input.split(", ").collect();

    let mut bearing = Bearing::NORTH;

    let mut path_taken = vec![];
    let mut current_position = Position::new();
    let mut repeat_pos = Position::new();
    let mut found = false;

    for step in directions {
        let (dir_change, len) = step.split_at(1);
        let len = len.parse::<i32>().unwrap();

        bearing = calc_new_bearing(bearing, dir_change);

        for _ in 0..len {
            match bearing {
                Bearing::NORTH => current_position.go_north(),
                Bearing::EAST => current_position.go_east(),
                Bearing::SOUTH => current_position.go_south(),
                Bearing::WEST => current_position.go_west(),
            }
            if !found {
                if path_taken.contains(&current_position) {
                    found = true;
                    repeat_pos = current_position.clone();
                } else {
                    path_taken.push(current_position.clone());
                }
            }
        }
        if found {
            break;
        }
    }

    println!(
        "Easter Bunny is {:?} blocks away!",
        current_position.x.abs() + current_position.y.abs()
    );
    println!(
        "First first location you visit twice is {:?} blocks away!",
        repeat_pos.x.abs() + repeat_pos.y.abs()
    );
}
