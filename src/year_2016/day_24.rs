use astar::{astar, SearchProblem};
use permutohedron::Heap;
use std::vec::IntoIter;

struct GridState {
    grid: Vec<Vec<char>>,
    start: (i32, i32),
    end: (i32, i32),
}

impl SearchProblem for GridState {
    type Node = (i32, i32);
    type Cost = i32;
    type Iter = IntoIter<((i32, i32), i32)>;

    fn start(&self) -> (i32, i32) {
        self.start
    }
    fn is_end(&self, other: &(i32, i32)) -> bool {
        other == &self.end
    }
    fn heuristic(&self, &(p_x, p_y): &(i32, i32)) -> i32 {
        let (s_x, s_y) = self.end;
        (s_x - p_x).abs() + (s_y - p_y).abs()
    }
    fn neighbors(&mut self, &(x, y): &(i32, i32)) -> IntoIter<((i32, i32), i32)> {
        let mut vec = vec![];
        for &(dx, dy) in [(1, 0), (-1, 0), (0, 1), (0, -1)].iter() {
            let nx = x + dx;
            let ny = y + dy;

            if let Some(row) = self.grid.get(ny as usize) {
                if let Some(&val) = row.get(nx as usize) {
                    if val != '#' {
                        vec.push(((x + dx, y + dy), 1));
                    }
                }
            }
        }
        vec.into_iter()
    }
}

fn find(grid: &Vec<Vec<char>>, ends_with_0: bool) -> usize {
    let mut position = vec![(-1, -1); 8];
    for y in 0..grid.len() {
        for x in 0..grid[y].len() {
            if grid[y][x] != '.' && grid[y][x] != '#' {
                // println!("{} {} is at {} {}", grid[y][x], grid[y][x] as u8 - 48, x, y);
                position[grid[y][x] as usize - 48] = (x as i32, y as i32);
            }
        }
    }

    let mut distances = vec![vec![0; 8]; 8];

    for a in 0..8 {
        for b in 0..8 {
            if distances[a][b] == 0 {
                let mut gs = GridState {
                    grid: grid.to_vec(),
                    start: position[a],
                    end: position[b],
                };
                let path = astar(&mut gs).expect("path not found");
                distances[a][b] = path.len() - 1;
                distances[b][a] = path.len() - 1;
            }
        }
    }

    let mut list = vec![1, 2, 3, 4, 5, 6, 7];
    let heap = Heap::new(&mut list);
    let mut min = 100000;
    for data in heap {
        let mut l = vec![0];
        l.append(&mut data.clone());
        if ends_with_0 {
            l.push(0);
        }
        let mut i = 0;
        let mut sum = 0;
        while i < l.len() - 1 {
            sum += distances[l[i]][l[i + 1]];
            i += 1;
        }
        if sum < min {
            min = sum;
        }
    }

    min
}

pub fn run() {
    let grid: Vec<Vec<char>> = super::input(24)
        .iter()
        .map(|line| line.chars().collect::<Vec<_>>())
        .collect();

    println!("p1 is {}", find(&grid, false));
    println!("p2 is {}", find(&grid, true));
}
