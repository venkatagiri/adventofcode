use regex::Regex;
use std::collections::HashMap;

pub fn run() {
    let input: Vec<String> = {
        use std::fs::File;
        use std::io::prelude::*;
        use std::io::BufReader;

        let file = BufReader::new(
            File::open("input/year_2016/day_10.txt").expect("file (input.txt) not found"),
        );
        file.lines().filter_map(|l| l.ok()).collect()
    };

    // Collect the instructions & process the input
    let mut instructions: HashMap<String, (String, String)> = HashMap::new();
    let re_inst =
        Regex::new(r"(?P<bot>.*) gives low to (?P<low>.*) and high to (?P<high>.*)").unwrap();

    for line in &input {
        if line.starts_with("bot") {
            let caps = re_inst.captures(line).unwrap();
            let bot = caps.name("bot").unwrap().into();
            let low = caps.name("low").unwrap().into();
            let high = caps.name("high").unwrap().into();
            instructions.insert(bot, (low, high));
        }
    }

    // Process the instructions
    let mut buckets: HashMap<String, Vec<u32>> = HashMap::new();
    let re_value = Regex::new(r"value (?P<value>.*) goes to (?P<recipient>.*)").unwrap();

    for line in &input {
        if line.starts_with("value") {
            let caps = re_value.captures(line).unwrap();
            let value = caps.name("value").unwrap().parse().unwrap();
            let recipient = caps.name("recipient").unwrap().into();

            let mut stack: Vec<(String, String, u32)> = vec![];
            stack.push(("input".to_string(), recipient, value));

            while let Some((giver, recipient, value)) = stack.pop() {
                buckets.remove(&giver);
                let list = buckets.entry(recipient.clone()).or_insert(vec![]);
                list.push(value);
                if list.len() == 2 {
                    let (low, high) = if list[0] < list[1] {
                        (list[0], list[1])
                    } else {
                        (list[1], list[0])
                    };
                    if low == 17 && high == 61 {
                        println!("bot is {}", recipient);
                    }
                    let rec1 = instructions.get(&recipient).unwrap().0.clone();
                    let rec2 = instructions.get(&recipient).unwrap().1.clone();
                    stack.push((recipient.clone(), rec2, high));
                    stack.push((recipient.clone(), rec1, low));
                }
            }
        }
    }

    println!(
        "xly is {}",
        buckets.get("output 0").unwrap()[0]
            * buckets.get("output 1").unwrap()[0]
            * buckets.get("output 2").unwrap()[0]
    );
}
