use regex::*;

fn fill_grid(grid: &mut Vec<Vec<char>>, width: usize, height: usize) {
    for h in 0..height {
        for w in 0..width {
            grid[h][w] = '*';
        }
    }
}

fn shift_row(grid: &mut Vec<Vec<char>>, row: usize, pixels: usize) {
    for _ in 0..pixels {
        let ch = grid[row].pop().unwrap();
        grid[row].insert(0, ch);
    }
}

fn positive_modulo(i: i32, n: i32) -> i32 {
    return (i % n + n) % n;
}

fn shift_col(grid: &mut Vec<Vec<char>>, col: usize, pixels: usize) {
    let len = grid.len();
    let mut col_vals = vec![];
    for row in 0..len {
        col_vals.push(grid[row][col]);
    }
    for row in 0..len {
        grid[positive_modulo(row as i32 + pixels as i32, len as i32) as usize][col] = col_vals[row];
    }
}

pub fn run() {
    let input: Vec<String> = {
        use std::fs::File;
        use std::io::prelude::*;
        use std::io::BufReader;

        let file = BufReader::new(
            File::open("input/year_2016/day_8.txt").expect("file (input.txt) not found"),
        );
        file.lines().filter_map(|l| l.ok()).collect()
    };

    let mut grid = vec![vec![' '; 50]; 6];
    let re_row = Regex::new(r"rotate row y=(?P<row>\d+) by (?P<pixels>\d+)").unwrap();
    let re_col = Regex::new(r"rotate column x=(?P<col>\d+) by (?P<pixels>\d+)").unwrap();

    for line in &input {
        if line.starts_with("rect") {
            let parts = line.split(" ").collect::<Vec<&str>>();
            let dimensions = parts.last().unwrap();
            let dimensions = dimensions.split("x").collect::<Vec<&str>>();
            let width: usize = dimensions[0].parse().unwrap();
            let height: usize = dimensions[1].parse().unwrap();
            fill_grid(&mut grid, width, height);
        } else if line.starts_with("rotate row") {
            let caps = re_row.captures(line).unwrap();
            let row: usize = caps.name("row").unwrap().parse().unwrap();
            let pixels: usize = caps.name("pixels").unwrap().parse().unwrap();
            shift_row(&mut grid, row, pixels);
        } else if line.starts_with("rotate column") {
            let caps = re_col.captures(line).unwrap();
            let col: usize = caps.name("col").unwrap().parse().unwrap();
            let pixels: usize = caps.name("pixels").unwrap().parse().unwrap();
            shift_col(&mut grid, col, pixels);
        }
    }

    let mut count = 0;
    for row in &grid {
        println!("{}", row.iter().cloned().collect::<String>());
        count += row
            .iter()
            .fold(0u32, |sum, &val| sum + if val == '*' { 1 } else { 0 });
    }
    println!("{} pixels are lit", count);
}
