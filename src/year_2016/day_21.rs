use permutohedron::Heap;
use regex::Regex;
use std::collections::VecDeque;

fn scramble(password: &str, operations: &Vec<String>) -> String {
    let mut spass = password.chars().collect::<VecDeque<char>>();

    let re_swap_position = Regex::new(r"swap position (?P<x>.*) with position (?P<y>.*)").unwrap();
    let re_swap_letter = Regex::new(r"swap letter (?P<x>.*) with letter (?P<y>.*)").unwrap();
    let re_rotate_steps = Regex::new(r"rotate (?P<dir>.*) (?P<steps>.*) steps?").unwrap();
    let re_rotate_position = Regex::new(r"rotate based on position of letter (?P<x>.*)").unwrap();
    let re_reverse = Regex::new(r"reverse positions (?P<x>.*) through (?P<y>.*)").unwrap();
    let re_move = Regex::new(r"move position (?P<x>.*) to position (?P<y>.*)").unwrap();

    for operation in operations {
        if let Some(caps) = re_swap_position.captures(operation) {
            let x = caps.name("x").unwrap().parse::<usize>().unwrap();
            let y = caps.name("y").unwrap().parse::<usize>().unwrap();
            spass.swap(x, y);
        } else if let Some(caps) = re_swap_letter.captures(operation) {
            let x = caps.name("x").unwrap().chars().next().unwrap();
            let y = caps.name("y").unwrap().chars().next().unwrap();

            let pos_x = spass.iter().position(|&v| v == x).unwrap();
            let pos_y = spass.iter().position(|&v| v == y).unwrap();
            spass.swap(pos_x, pos_y);
        } else if let Some(caps) = re_rotate_steps.captures(operation) {
            let dir = caps.name("dir").unwrap();
            let steps = caps.name("steps").unwrap().parse::<usize>().unwrap();

            for _ in 0..steps {
                if dir == "left" {
                    let el = spass.pop_front().unwrap();
                    spass.push_back(el);
                } else {
                    let el = spass.pop_back().unwrap();
                    spass.push_front(el);
                }
            }
        } else if let Some(caps) = re_rotate_position.captures(operation) {
            let x = caps.name("x").unwrap().chars().next().unwrap();
            let pos_x = spass.iter().position(|&v| v == x).unwrap();
            let steps = 1 + pos_x + if pos_x >= 4 { 1 } else { 0 };

            for _ in 0..steps {
                let el = spass.pop_back().unwrap();
                spass.push_front(el);
            }
        } else if let Some(caps) = re_reverse.captures(operation) {
            let x = caps.name("x").unwrap().parse::<usize>().unwrap();
            let y = caps.name("y").unwrap().parse::<usize>().unwrap();
            for i in 0..(y + 1 - x) / 2 {
                let t = spass[y - i];
                spass[y - i] = spass[x + i];
                spass[x + i] = t;
            }
        } else if let Some(caps) = re_move.captures(operation) {
            let x = caps.name("x").unwrap().parse::<usize>().unwrap();
            let y = caps.name("y").unwrap().parse::<usize>().unwrap();

            let val = spass.remove(x).unwrap();
            spass.insert(y, val);
        } else {
            panic!("invalid operation! {}", operation);
        }
    }
    spass.into_iter().collect()
}

pub fn run() {
    assert_eq!(
        scramble(
            "abcdefgh",
            &vec!["swap position 5 with position 6".to_string()]
        ),
        "abcdegfh"
    );
    assert_eq!(
        scramble("abcdefgh", &vec!["swap letter c with letter h".to_string()]),
        "abhdefgc"
    );
    assert_eq!(
        scramble("abcdefgh", &vec!["rotate left 3 steps".to_string()]),
        "defghabc"
    );
    assert_eq!(
        scramble("abcdefgh", &vec!["rotate right 4 steps".to_string()]),
        "efghabcd"
    );
    assert_eq!(
        scramble("abcdefgh", &vec!["rotate right 1 step".to_string()]),
        "habcdefg"
    );
    assert_eq!(
        scramble(
            "abcdefgh",
            &vec!["rotate based on position of letter a".to_string()]
        ),
        "habcdefg"
    );
    assert_eq!(
        scramble(
            "abcdefgh",
            &vec!["rotate based on position of letter g".to_string()]
        ),
        "abcdefgh"
    );
    assert_eq!(
        scramble(
            "abcdefgh",
            &vec!["reverse positions 1 through 3".to_string()]
        ),
        "adcbefgh"
    );
    assert_eq!(
        scramble(
            "abcdefgh",
            &vec!["reverse positions 0 through 5".to_string()]
        ),
        "fedcbagh"
    );
    assert_eq!(
        scramble(
            "abcdefgh",
            &vec!["move position 0 to position 3".to_string()]
        ),
        "bcdaefgh"
    );

    let operations = super::input(21);
    println!("p1 is {}", scramble("abcdefgh", &operations));

    let mut chars: Vec<char> = "abcdefgh".chars().collect();
    let heap = Heap::new(&mut chars);
    for data in heap {
        let combo = data.clone().into_iter().collect::<String>();
        if scramble(&combo, &operations) == "fbgdceah" {
            println!("p2 is {}", combo);
            break;
        }
    }
}
