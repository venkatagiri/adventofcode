fn find(grid: &Vec<Vec<char>>, input: &Vec<String>) {
    let mut pos_right: i32 = 0;
    let mut pos_down: i32 = 2;
    let mut path_taken = vec![];

    for line in input {
        for dir in line.chars() {
            match dir {
                'L' if pos_right > 0
                    && grid[pos_down as usize][(pos_right - 1) as usize] != '-' =>
                {
                    pos_right -= 1
                }
                'R' if pos_right < (grid.len() - 1) as i32
                    && grid[pos_down as usize][(pos_right + 1) as usize] != '-' =>
                {
                    pos_right += 1
                }
                'U' if pos_down > 0 && grid[(pos_down - 1) as usize][pos_right as usize] != '-' => {
                    pos_down -= 1
                }
                'D' if pos_down < (grid.len() - 1) as i32
                    && grid[(pos_down + 1) as usize][pos_right as usize] != '-' =>
                {
                    pos_down += 1
                }
                _ => continue,
            }
        }
        path_taken.push(grid[pos_down as usize][pos_right as usize]);
    }
    let key_code: String = path_taken.iter().fold("".to_string(), |mut a, b| {
        a.push(*b);
        a
    });
    println!("key code is {}", key_code);
}

pub fn run() {
    let input = {
        use std::fs::File;
        use std::io::prelude::*;
        use std::io::BufReader;

        let file = BufReader::new(File::open("input/year_2016/day_2.txt").unwrap());
        let mut lines = vec![];
        for line in file.lines() {
            lines.push(line.unwrap());
        }
        lines
    };

    // grid for 1st part
    let grid1 = vec![
        vec!['1', '2', '3'],
        vec!['4', '5', '6'],
        vec!['7', '8', '9'],
    ];
    find(&grid1, &input);

    // grid for 2nd part
    let grid2 = vec![
        vec!['-', '-', '1', '-', '-'],
        vec!['-', '2', '3', '4', '-'],
        vec!['5', '6', '7', '8', '9'],
        vec!['-', 'A', 'B', 'C', '-'],
        vec!['-', '-', 'D', '-', '-'],
    ];
    find(&grid2, &input);
}
