fn tran(x: &str) -> usize {
    match x {
        "a" => 0,
        "b" => 1,
        "c" => 2,
        "d" => 3,
        _ => 999,
    }
}

fn val(registers: &Vec<i32>, x: &str) -> i32 {
    if tran(x) < 5 {
        registers[tran(x)]
    } else {
        x.parse::<i32>().unwrap()
    }
}

fn find(input: &Vec<String>, reg: Vec<i32>) {
    let len = input.len() as i32;
    let mut registers = reg.clone();
    let mut x = 0;
    let mut loopcount = 0;
    while x < len {
        // println!("registers are {:?}", registers);
        loopcount += 1;
        if loopcount > 100000 {
            // panic!("too many loops");
        }
        // let instruction = input[x];
        let code = input[x as usize]
            .split(" ")
            .collect::<Vec<&str>>()
            .first()
            .unwrap()
            .clone();
        // println!("step is {}, instruction is {}", x, input[x as usize]);
        match code {
            "cpy" => {
                let from = input[x as usize].split(" ").collect::<Vec<&str>>()[1].clone();
                let to = input[x as usize].split(" ").collect::<Vec<&str>>()[2].clone();
                registers[tran(to)] = val(&registers, from);
            }
            "inc" => {
                let val = input[x as usize]
                    .split(" ")
                    .collect::<Vec<&str>>()
                    .last()
                    .unwrap()
                    .clone();
                let register = tran(val);
                registers[register] += 1;
            }
            "dec" => {
                let val = input[x as usize]
                    .split(" ")
                    .collect::<Vec<&str>>()
                    .last()
                    .unwrap()
                    .clone();
                let register = tran(val);
                registers[register] -= 1;
            }
            "jnz" => {
                let y = input[x as usize].split(" ").collect::<Vec<&str>>()[1].clone();
                let steps = input[x as usize].split(" ").collect::<Vec<&str>>()[2]
                    .clone()
                    .parse::<i32>()
                    .unwrap();
                if val(&registers, y) != 0 {
                    x += steps;
                    continue;
                }
            }
            _ => println!("others"),
        }
        x += 1;
    }
    println!("registers are {:?}", registers);
}

pub fn run() {
    let input: Vec<String> = {
        use std::fs::File;
        use std::io::prelude::*;
        use std::io::BufReader;

        let file = BufReader::new(
            File::open("input/year_2016/day_12.txt").expect("file (input.txt) not found"),
        );
        file.lines().filter_map(|l| l.ok()).collect()
    };

    find(&input, vec![0, 0, 0, 0]);
    find(&input, vec![0, 0, 1, 0]);
}
