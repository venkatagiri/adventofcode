fn find(input: &String, rows: usize) -> u32 {
    let mut tiles: Vec<char> = input.chars().collect();
    let mut safe = tiles
        .iter()
        .fold(0u32, |sum, &val| sum + if val == '.' { 1 } else { 0 });

    for _ in 1..rows {
        let mut new_tile = vec![];
        for y in 0..tiles.len() {
            let left = if y == 0 { '.' } else { tiles[y - 1] };
            let center = tiles[y];
            let right = if y + 1 == tiles.len() {
                '.'
            } else {
                tiles[y + 1]
            };
            // Rules
            if left == '^' && center == '^' && right == '.' {
                new_tile.push('^');
            } else if left == '.' && center == '^' && right == '^' {
                new_tile.push('^');
            } else if left == '^' && center == '.' && right == '.' {
                new_tile.push('^');
            } else if left == '.' && center == '.' && right == '^' {
                new_tile.push('^');
            } else {
                new_tile.push('.');
            }
        }
        tiles = new_tile;
        safe += tiles
            .iter()
            .fold(0u32, |sum, &val| sum + if val == '.' { 1 } else { 0 });
    }
    safe
}

pub fn run() {
    let input = {
        use std::fs::File;
        use std::io::prelude::*;
        use std::io::BufReader;

        let mut file = BufReader::new(File::open("input/year_2016/day_18.txt").unwrap());
        let mut line = String::new();
        file.read_line(&mut line).unwrap();
        line
    };

    println!("p1 is {}", find(&input, 40));
    println!("p2 is {}", find(&input, 400000));
}
