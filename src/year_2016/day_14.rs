use crypto::digest::Digest;
use crypto::md5::Md5;

use std::collections::HashMap;
use std::i32;

static KEY: &'static [u8; 8] = b"yjdafjpo";
static NTH: i32 = 64;

fn hash(n: i32, loops: i32) -> Vec<char> {
    let mut hasher = Md5::new();

    hasher.input(KEY);
    hasher.input(&n.to_string().as_bytes());
    let mut result = hasher.result_str();

    for _ in 0..loops {
        hasher.reset();
        hasher.input_str(&result);
        result = hasher.result_str()
    }

    result.chars().collect::<Vec<char>>()
}

fn triple(chars: &Vec<char>) -> Option<char> {
    for x in chars.windows(3) {
        if x[0] == x[1] && x[1] == x[2] {
            return Some(x[0]);
        }
    }
    None
}

fn has_quintuple(mtch: &char, chars: &Vec<char>) -> bool {
    for x in chars.windows(5) {
        if *mtch == x[0] && x[0] == x[1] && x[1] == x[2] && x[2] == x[3] && x[3] == x[4] {
            return true;
        }
    }
    false
}

fn find(loops: i32) -> i32 {
    let mut hash_cache: HashMap<i32, Vec<char>> = HashMap::new();
    let mut found = 0;

    for n in 0.. {
        let h = hash_cache
            .entry(n)
            .or_insert_with(|| hash(n, loops))
            .clone();
        if let Some(mtch) = triple(&h) {
            for m in n + 1..n + 1001 {
                let h2 = hash_cache.entry(m).or_insert_with(|| hash(m, loops));
                if has_quintuple(&mtch, &h2) {
                    found += 1;
                    break;
                }
            }
        }
        if found == NTH {
            return n;
        }
    }
    0
}

pub fn run() {
    println!("p1 is {}", find(0));
    println!("p2 is {}", find(2016));
}
