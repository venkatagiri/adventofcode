fn is_abba(entry: &str) -> bool {
    for x in entry.chars().collect::<Vec<_>>().windows(4) {
        if x[0] == x[3] && x[1] == x[2] && x[0] != x[1] {
            return true;
        }
    }
    false
}

fn get_aba(entry: &str) -> Vec<String> {
    let mut list = vec![];
    for x in entry.chars().collect::<Vec<_>>().windows(3) {
        if x[0] == x[2] && x[0] != x[1] {
            list.push(x.iter().map(|&c| c).collect());
        }
    }
    list
}

pub fn run() {
    let input: Vec<String> = {
        use std::fs::File;
        use std::io::prelude::*;
        use std::io::BufReader;

        let file = BufReader::new(
            File::open("input/year_2016/day_7.txt").expect("file (input.txt) not found"),
        );
        file.lines().filter_map(|l| l.ok()).collect()
    };

    let mut count = 0;
    for x in &input {
        let entries = x.split(|c| c == '[' || c == ']').collect::<Vec<&str>>();
        let mut valid = false;
        for (index, entry) in entries.iter().enumerate() {
            if is_abba(entry) {
                if index % 2 == 0 {
                    valid = true;
                } else {
                    valid = false;
                    break;
                }
            }
        }
        if valid {
            count += 1;
        }
    }
    println!("part1: {} IPs support tls", count);

    count = 0;
    for x in &input {
        let entries = x.split(|c| c == '[' || c == ']').collect::<Vec<&str>>();
        let mut outside = vec![];
        let mut inside = vec![];
        for (index, entry) in entries.iter().enumerate() {
            if index % 2 == 0 {
                outside.append(&mut get_aba(entry));
            } else {
                inside.append(&mut get_aba(entry));
            }
        }
        for aba in &outside {
            let mut bab = String::new();
            let aba: Vec<char> = aba.chars().collect();
            bab.push(aba[1]);
            bab.push(aba[0]);
            bab.push(aba[1]);
            if inside.contains(&bab) {
                count += 1;
                break;
            }
        }
    }
    println!("part2: {} IPs support ssl", count);
}
