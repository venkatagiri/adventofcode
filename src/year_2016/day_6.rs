use std::collections::HashMap;

pub fn run() {
    let input = {
        use std::fs::File;
        use std::io::prelude::*;
        use std::io::BufReader;

        let file = BufReader::new(File::open("input/year_2016/day_6.txt").unwrap());
        let mut lines = vec![];
        for line in file.lines() {
            let line = line.unwrap();
            lines.push(line);
        }
        lines
    };

    let mut scratch = vec![];
    let no_of_chars = input[0].len();

    for _ in 0..no_of_chars {
        scratch.push(HashMap::new());
    }

    for line in input {
        for (pos, ch) in line.chars().enumerate() {
            let mut map = scratch.get_mut(pos).unwrap();
            let counter = map.entry(ch).or_insert(0);
            *counter += 1;
        }
    }

    let mut ans1 = vec![];
    let mut ans2 = vec![];
    for coll in scratch {
        let mut list: Vec<(char, i32)> = coll.iter().map(|(k, v)| (*k, *v)).collect();
        list.sort_by_key(|&(_, count)| count);
        list.reverse();
        let (letter1, _) = list[0];
        let (letter2, _) = list[25];
        ans1.push(letter1.to_string());
        ans2.push(letter2.to_string());
    }

    println!("part 1 is {:?}", ans1.join(""));
    println!("part 2 is {:?}", ans2.join(""));
}
