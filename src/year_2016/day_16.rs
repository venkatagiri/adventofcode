fn checksum(input: &String) -> String {
    let cksum: String = input
        .chars()
        .collect::<Vec<_>>()
        .chunks(2)
        .map(|c| if c[0] == c[1] { '1' } else { '0' })
        .collect();
    if cksum.len() % 2 == 0 {
        checksum(&cksum)
    } else {
        cksum
    }
}

fn gen(input: &str, disk_length: usize) -> String {
    let mut output = String::from(input);
    while (output.len()) < disk_length {
        let b: String = output
            .chars()
            .rev()
            .map(|c| if c == '0' { '1' } else { '0' })
            .collect();
        output = format!("{}0{}", output, b);
    }
    (&output[0..disk_length]).into()
}

pub fn run() {
    println!("p1 is {}", checksum(&gen("10111011111001111", 272)));
    println!("p2 is {}", checksum(&gen("10111011111001111", 35651584)));
}
