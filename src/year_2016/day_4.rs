use regex::Regex;
use std::cmp::{Ord, Ordering};
use std::collections::BTreeMap;

#[derive(Debug)]
struct Room {
    name: String,
    sector_id: u32,
    checksum: String,
}

impl Room {
    fn is_real(&self) -> bool {
        // Find the letter counts
        let mut letter_count: BTreeMap<char, u32> = BTreeMap::new();
        for ch in self.name.chars() {
            if ch == '-' {
                continue;
            }
            let counter = letter_count.entry(ch).or_insert(0);
            *counter += 1;
        }

        // Sort the letter counts
        let mut lc: Vec<LCount> = letter_count
            .into_iter()
            .map(|(k, v)| LCount {
                letter: k,
                count: v,
            })
            .collect();
        lc.sort();

        // Calc the checksum
        let mut cs = String::new();
        for x in 0..5 {
            cs.push(lc[x].letter);
        }

        // Check if it's real
        cs == self.checksum
    }

    fn decrypted_name(&self) -> String {
        let mut name = String::new();
        for ch in self.name.chars() {
            if ch == '-' {
                name.push(' ');
                continue;
            }
            let dec_ch = (((ch as u32) - 97 + self.sector_id) % 26) as u8;
            let dec_ch = (dec_ch + 97) as char;
            name.push(dec_ch);
        }
        name
    }
}

#[derive(Debug, Eq, PartialEq, PartialOrd)]
struct LCount {
    letter: char,
    count: u32,
}

impl Ord for LCount {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.count > other.count {
            Ordering::Less
        } else if self.count < other.count {
            Ordering::Greater
        } else if self.letter < other.letter {
            Ordering::Less
        } else {
            Ordering::Greater
        }
    }
}

pub fn run() {
    // Parsing Input
    let rooms: Vec<Room> = {
        use std::fs::File;
        use std::io::prelude::*;
        use std::io::BufReader;

        let file = BufReader::new(File::open("input/year_2016/day_4.txt").unwrap());
        let mut rooms = vec![];

        let re = Regex::new(r"^([a-z-]+)(\d+)\[([a-z]+)\]$").unwrap();
        for line in file.lines() {
            let line = line.unwrap();
            let caps = re.captures(&line).unwrap();
            let r = Room {
                name: caps.at(1).unwrap().to_string(),
                sector_id: caps.at(2).unwrap().parse::<u32>().unwrap(),
                checksum: caps.at(3).unwrap().to_string(),
            };
            rooms.push(r);
        }

        rooms
    };

    // Find the valid rooms
    let sum_of_sector_ids = rooms
        .iter()
        .filter(|r| r.is_real())
        .fold(0, |sum, r| sum + r.sector_id);
    println!("sum of real room sector_ids is {:?}", sum_of_sector_ids);

    if let Some(room) = rooms
        .iter()
        .filter(|r| r.is_real())
        .find(|&&ref room| room.decrypted_name() == "northpole object storage ")
    {
        println!(
            "sector id of northpole object storage is {}",
            room.sector_id
        );
    }
}
