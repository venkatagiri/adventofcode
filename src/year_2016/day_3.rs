fn is_valid_triangle(sides: &Vec<u32>) -> bool {
    if sides[0] + sides[1] > sides[2]
        && sides[1] + sides[2] > sides[0]
        && sides[2] + sides[0] > sides[1]
    {
        true
    } else {
        false
    }
}

fn part1(input: &Vec<String>) {
    let mut lines = vec![];
    for line in input {
        let mut nums: Vec<u32> = vec![];
        for part in line.split(" ").collect::<Vec<&str>>() {
            if !part.is_empty() {
                nums.push(part.parse::<u32>().unwrap());
            }
        }
        lines.push(nums);
    }

    let mut valid = 0;
    for i in &lines {
        if is_valid_triangle(i) {
            valid += 1;
        }
    }
    println!("p1 is {}", valid);
}

fn part2(input: &Vec<String>) {
    let mut lines = vec![];
    let mut v: Vec<Vec<u32>> = vec![vec![], vec![], vec![]];
    for line in input {
        let mut ind = 0;
        for part in line.split(" ").collect::<Vec<&str>>() {
            if !part.is_empty() {
                v[ind].push(part.parse::<u32>().unwrap());
                ind += 1;
            }
        }
        if v[0].len() == 3 {
            for x in 0..3 {
                lines.push(v[x].clone());
                v[x].clear();
            }
        }
    }

    let mut valid = 0;
    for i in &lines {
        if is_valid_triangle(i) {
            valid += 1;
        }
    }
    println!("p2 is {}", valid);
}

pub fn run() {
    let input = {
        use std::fs::File;
        use std::io::prelude::*;
        use std::io::BufReader;

        let file = BufReader::new(File::open("input/year_2016/day_3.txt").unwrap());
        file.lines().filter_map(|l| l.ok()).collect()
    };

    part1(&input);
    part2(&input);
}
