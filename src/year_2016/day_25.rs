fn tran(x: &str) -> usize {
    match x {
        "a" => 0,
        "b" => 1,
        "c" => 2,
        "d" => 3,
        _ => 999,
    }
}

fn val(registers: &Vec<i32>, x: &str) -> i32 {
    if tran(x) < 5 {
        registers[tran(x)]
    } else {
        x.parse().unwrap()
    }
}

fn find(input: &Vec<String>, reg: Vec<i32>) -> Vec<i32> {
    let mut instructions: Vec<(&str, &str, &str)> = input
        .iter()
        .map(|ins| {
            let parts = ins.split(" ").collect::<Vec<&str>>();
            let code = parts[0].clone();
            let arg1 = parts[1].clone();
            let arg2 = if parts.len() == 3 {
                parts.last().unwrap().clone()
            } else {
                ""
            };
            (code, arg1, arg2)
        })
        .collect();

    let len = instructions.len() as i32;
    let mut registers = reg.clone();
    let mut pc = 0;

    let mut signal = vec![];

    while pc < len {
        let code = instructions[pc as usize].0;
        let arg1 = instructions[pc as usize].1;
        let arg2 = instructions[pc as usize].2;
        match code {
            "cpy" => {
                if tran(arg2) < 999 {
                    registers[tran(arg2)] = val(&registers, arg1);
                }
            }
            "inc" => {
                registers[tran(arg1)] += 1;
            }
            "dec" => {
                registers[tran(arg1)] -= 1;
            }
            "jnz" => {
                let y = arg1;
                let steps = val(&registers, arg2);
                if val(&registers, y) != 0 {
                    pc += steps;
                    continue;
                }
            }
            "tgl" => {
                let offset = val(&registers, arg1);
                if 0 <= pc + offset && pc + offset < len {
                    let code = instructions[(pc + offset) as usize].0;
                    let arg2 = instructions[(pc + offset) as usize].2;
                    let ncode = if code == "inc" {
                        "dec"
                    } else if arg2 == "" {
                        "inc"
                    } else if code == "jnz" {
                        "cpy"
                    } else {
                        "jnz"
                    };
                    instructions[(pc + offset) as usize].0 = ncode;
                }
            }
            "out" => {
                let val = val(&registers, arg1);
                signal.push(val);
                if signal.len() == 100 {
                    break;
                }
            }
            _ => println!("others"),
        }
        pc += 1;
    }
    signal
}

pub fn run() {
    let input = super::input(25);

    for a in 0.. {
        let signal = find(&input, vec![a, 0, 0, 0]);
        let nos = signal
            .chunks(2)
            .filter(|x| x[0] == 0 && x[1] == 1)
            .collect::<Vec<&[i32]>>()
            .len();
        if nos == 50 {
            println!("a is {}", a);
            break;
        }
    }
}
