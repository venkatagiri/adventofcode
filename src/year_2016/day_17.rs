use crypto::digest::Digest;
use crypto::md5::Md5;

use std::collections::VecDeque;
use std::vec::IntoIter;

static W: i32 = 4;
static H: i32 = 4;

fn hash(key: &String) -> String {
    let mut hasher = Md5::new();
    hasher.input_str(key);
    hasher.result_str()
}

fn is_open(passcode: &String, dir: char) -> bool {
    let chars = hash(&passcode).chars().collect::<Vec<_>>();
    (match dir {
        'U' => chars[0],
        'D' => chars[1],
        'L' => chars[2],
        'R' => chars[3],
        _ => panic!("invalid direction"),
    }) > 'a'
}

fn neighbours(key: &String, &(x, y): &(i32, i32)) -> IntoIter<(i32, i32, String)> {
    let mut vec = vec![];
    for &(dx, dy, dir) in [(0, -1, 'U'), (0, 1, 'D'), (1, 0, 'R'), (-1, 0, 'L')].iter() {
        let mut passcode = key.clone();
        passcode.push(dir);
        if (x + dx) >= 0 && (y + dy) >= 0 && (x + dx) < W && (y + dy) < H && is_open(&key, dir) {
            vec.push((x + dx, y + dy, passcode));
        }
    }
    vec.into_iter()
}

fn find(key: &str, find_longest: bool) -> String {
    let mut queue = VecDeque::new();
    queue.push_back((0, 0, key.to_string()));

    let mut soln = String::new();

    while let Some((x, y, passcode)) = queue.pop_front() {
        for (x2, y2, passcode) in neighbours(&passcode, &(x, y)) {
            if x2 == W - 1 && y2 == H - 1 {
                let s = passcode.split_at(key.len()).1;
                if !find_longest {
                    return s.to_string();
                }
                if s.len() > soln.len() {
                    soln = s.to_string();
                }
            } else {
                queue.push_back((x2, y2, passcode.clone()));
            }
        }
    }
    soln
}

pub fn run() {
    println!("p1 is {:?}", find("qzthpkfp", false));
    println!("p2 is {:?}", find("qzthpkfp", true).len());
}
