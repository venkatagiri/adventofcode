use std::io;
use std::net;
use std::num;
use std::result;

#[derive(Debug)]
pub enum Error {
    AddrParseError(net::AddrParseError),
    Io(io::Error),
    Regex(regex::Error),
    ParseIntError(num::ParseIntError),
}

impl From<net::AddrParseError> for Error {
    fn from(other: net::AddrParseError) -> Self {
        Error::AddrParseError(other)
    }
}

impl From<io::Error> for Error {
    fn from(other: io::Error) -> Self {
        Error::Io(other)
    }
}

impl From<regex::Error> for Error {
    fn from(other: regex::Error) -> Self {
        Error::Regex(other)
    }
}

impl From<num::ParseIntError> for Error {
    fn from(other: num::ParseIntError) -> Self {
        Error::ParseIntError(other)
    }
}

pub type Result<T> = result::Result<T, Error>;
