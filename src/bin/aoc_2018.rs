extern crate adventofcode;
extern crate dotenv;

use adventofcode::year_2018::*;

fn main() {
    dotenv::dotenv().ok();

    let input = std::env::args().nth(1).unwrap_or("4".into());
    let input = input.parse::<u32>().unwrap_or(1);

    match input {
        1 => day_1::run(),
        2 => day_2::run(),
        3 => day_3::run(),
        4 => day_4::run(),
        _ => println!("invalid day!"),
    }
}
