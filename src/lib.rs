extern crate astar;
extern crate crypto;
extern crate curl;
extern crate permutohedron;
extern crate regex;

pub fn input(year: u32, day: u32) -> Vec<String> {
    let mut data = Vec::new();
    let mut handle = curl::easy::Easy::new();
    handle
        .url(&format!(
            "https://adventofcode.com/{}/day/{}/input",
            year, day
        ))
        .unwrap();
    handle
        .cookie(&std::env::var("AOC_COOKIE").expect("env var AOC_COOKIE not set"))
        .unwrap();
    {
        let mut transfer = handle.transfer();
        transfer
            .write_function(|new_data| {
                data.extend_from_slice(new_data);
                Ok(new_data.len())
            })
            .unwrap();
        transfer.perform().unwrap();
    }
    let response = std::str::from_utf8(&data).unwrap();

    response.lines().map(|l| l.into()).collect()
}

pub mod error;

// every year
pub mod year_2016;
pub mod year_2018;
